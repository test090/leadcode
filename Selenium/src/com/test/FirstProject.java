package com.test;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import java.io.*;
import java.util.Properties;

public class FirstProject {
	String s1 = System.setProperty("webdriver.chrome.driver", "D:\\Users\\kctech5\\Downloads\\chromedriver_win32\\chromedriver.exe");
	WebDriver wd = new ChromeDriver();

public void URL(String url)throws Exception{
	wd.get(url);
	wd.manage().window().maximize();
	Thread.sleep(3000);
}
public void login(String username,String password) throws Exception {
	wd.findElement(By.name("UserID")).sendKeys(username);
	Thread.sleep(2000);
	wd.findElement(By.name("Password")).sendKeys(password);
	Thread.sleep(2000);
	JavascriptExecutor js1 = (JavascriptExecutor) wd;
	WebElement element1 = wd.findElement(By.xpath("//button/span"));
    ((JavascriptExecutor)wd).executeScript("arguments[0].scrollIntoView();", element1);
	wd.findElement(By.xpath("//button/span")).click();
	Thread.sleep(2000);
	wd.findElement(By.xpath("//div/div[text()='Got it']")).click();
	Thread.sleep(2000);
}
public void adminapp(String email)throws Exception {
	wd.findElement(By.id("appAdmin_menu")).click();
	Thread.sleep(2000);
	wd.findElement(By.xpath("//tbody/tr/td[text()='Update User']")).click();
	Thread.sleep(2000);
	wd.findElement(By.name("EmailId")).sendKeys(email);
	Thread.sleep(2000);
	wd.findElement(By.xpath("//tbody/tr/td/div[text()='Search']")).click();
	Thread.sleep(2000);
}
public void userselect(String lastname,String firstname)throws Exception {
	wd.findElement(By.xpath("//tbody/tr/td/div[text()='"+lastname+"']//following::td/div[text()='"+firstname+"']")).click();
	Thread.sleep(2000);
}
public void getfnln()throws Exception {
	WebElement firstname = wd.findElement(By.xpath("//tr/td/nobr/label[text()='First Name']//following::td//input"));
	System.out.println("User First Name " + firstname.getAttribute("value"));
	WebElement lastname = wd.findElement(By.xpath("//tr/td/nobr/label[text()='Last Name']//following::td//input"));
	System.out.println("User Last Name " + lastname.getAttribute("value"));
	Thread.sleep(2000);
}
public void selectuser()throws Exception{
	JavascriptExecutor js = (JavascriptExecutor) wd;
	WebElement element = wd.findElement(By.xpath("//tr/td/nobr/label[text()='Select User']//following::span/img[@class='comboBoxItemPicker']"));
    ((JavascriptExecutor)wd).executeScript("arguments[0].scrollIntoView();", element);
	Thread.sleep(2000);
	wd.findElement(By.xpath("//tr/td/nobr/label[text()='Select User']//following::span/img[@class='comboBoxItemPicker']")).click();
	Thread.sleep(2000);
}
}
