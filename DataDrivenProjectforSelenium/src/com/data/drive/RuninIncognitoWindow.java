package com.data.drive;

import java.util.*;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;

public class RuninIncognitoWindow {
	static WebDriver wd;
	static Actions a;
	static Random r;
	static JavascriptExecutor js;
	public void one() {
	    String s1 = System.setProperty("webdriver.chrome.driver", "D:\\Users\\kctech5\\Downloads\\chromedriver_win32\\chromedriver.exe");
	    ChromeOptions options = new ChromeOptions();
	    options.addArguments("--incognito");
	    wd = new ChromeDriver(options);
	    a = new Actions(wd);
	    r = new Random();
	}
	private String url(String URL) {
		wd.get(URL);
		wd.manage().window().maximize();
		System.out.println("Window size is " + wd.manage().window().getSize());
		return URL;
	}
	public static void main(String[] args) throws Exception {
		RuninIncognitoWindow riw = new RuninIncognitoWindow();
	    riw.one();
	    riw.url("https://seleniumeasy.com/test");
	}
}
