package com.logical.programs;

import java.io.*;
import java.util.*;

public class DigitsAdd {
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		System.out.println("Enter the number");
		long n=s.nextLong();
		long sum = n;
		while(n>9) {
		sum=0;
		while(n>0) {
			long r=n%10;
			sum+=r;
			n/=10;
		}
		n=sum;
		}
		System.out.println("Sum of the value is " + sum);
	}

}
